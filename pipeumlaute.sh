#!/bin/bash
#filter (pipe) script für mg.mud.de um es sinnvoll per espeak zu verarbeiten
#version 0.3

#how to use:
#terminal 1:
##cd somewhere-you-like-to-save-eg:
#cd /tmp
#script -f mg170326
#telnet mg.mud.de

#terminal 2
#tail -f mg170326 |pipeumlaute.sh|espeak -v de -s 200 

sed -e 's/A[Ee]/Ä/g' -e 's/ae/ä/g' -e 's/U[Ee]/Ü/g' -e 's/ue/ü/g' -e 's/O[Ee]/Ö/g' -e 's/oe/ö/g' -e 's/sz/ß/g' 
